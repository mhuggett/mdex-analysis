### MDEX ANALYSIS SETTINGS

# Set the path to user-session data dir
path_sessions = /home/asus/sam_sessions/sam_results/sam_results_170422/

# Analysis output dir
dir_logs = logs/
dir_results = results/

file_header_raw = header_raw.txt
file_header_sparse = header_sparse.txt

file_log_raw = _log_raw_
file_log_sparse = _log_sparse_

file_csv_raw = user_results_raw.csv
file_csv_sparse = user_results_sparse.csv
