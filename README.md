
Project **mdex_analysis** parses event.txt for all users who completed the mdex usability study into a final sparse table of results per task.
<br>


**App_count**  
Guages the completeness of each user session, e.g. whether questionnaires have been submitted, or if there are missing/extra tasks in the user session.

Run App_count with the script `run-counts`
<br>

**App_events_to_raw**  
Extracts the Demographic questionaire and all tasks done by each user into a simple **raw_results** CSV. Each row represents a user. The columns L to R show the demographic and UE surveys, and the user's tasks in the order they were done. Each task includes columns for start/end times, user responses, and events for each task.  

- Path to input user session data is set in data/config.txt  
- Run events-to-raw with the script `run-e2r`
- Output CSV file will appear in the dir data/results/
- Import the CSV to Excel to check
- Fix parser bugs, or issues in the input data, and re-run

Run App_events_to_raw with the script `run-e2r`
<br>

**App_raw_to_sparse**  
Converts the raw_results CSV into a sparse table, wherein the table columns show all tasks assigned in the study sorted by task ID. Columns for tasks not performed by a user are left empty.

Run App_raw_to_sparse with the script `run-r2s`
<br>

--
