package mdex.analysis.sessions;

import java.io.IOException;

import mdex.analysis.Locals;
import mdex.util.Log;
import mdex.util.SystemUtils;
import mdex.util.TimeUtils;

public class Util {

  /**
   * Initializes Log path, sets global entities, and loads configs at start of run.
   * 
   * @param log_name
   * @param port
   */
  static void init(String log_name) throws Exception {
    try {
      String filepath = Locals.dir_logs + log_name + TimeUtils.getDateYMD() + ".txt";
      new Log(filepath, true);
      SystemUtils.print_banner();

    } catch (IOException e) {
      Log.println(e);
      Log.exit(); // Fatal : can't continue w/o logging.
    }
  }

}
