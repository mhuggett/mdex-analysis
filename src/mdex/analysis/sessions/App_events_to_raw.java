package mdex.analysis.sessions;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.RandomAccessFile;
import java.util.Iterator;
import java.util.TreeMap;
import java.util.TreeSet;

import mdex.analysis.Locals;
import mdex.util.Log;
import mdex.util.TimeUtils;


/**
 * Generates a CSV table from input user logs. One row per user, with each column representing a factor of the user
 * study (e.g. an answer to a survey question, a timestamp, or a blob of UI actions).
 * <p>
 * RandomAccessFile is used for reading input, BufferedWriter for writing output.
 * 
 * @author mhuggett
 */

public class App_events_to_raw {

  /** Shows all parameters collected by the experimental UI. Useful for debugging. */
  static TreeSet<String> allParams = new TreeSet<>();

  private static BufferedWriter bw;
  private static String file_events = "events.txt";
  private static String file_q_demographic = "Demographic.txt";
  private static String file_q_userExperience = "User_Experience.txt";

  private static String p_action = "action";

  private static String path_sessions;


  private static String clean_line(String line) {
    line = line.trim();
    return line;
  }


  private static String get_date(String line) {
    String[] date_time = line.split("_UTC")[0].split("_");
    return date_time[0].split(" ")[1] + "_" + date_time[1];
  }


  private static long get_millis(String line) {
    String[] date_time = line.split("_UTC")[0].split("_");
    return toMillis(date_time[1]);
  }


  // Not used.
  static void getCompletion(String filepath,
                            String tag_end_session) throws Exception {

    // Events: last line ends with 'Thanks_XXX.jsp'
    BufferedReader br = new BufferedReader(new FileReader(filepath + file_events));
    String line = "";
    String last_line = "";
    while ((line = br.readLine()) != null)
      last_line = line;
    br.close();

    if (last_line.endsWith(tag_end_session))
      bw.write("\tT");
    else
      bw.write("\tF");

    // Status: numTasks == taskNum
    try {
      br = new BufferedReader(new FileReader(filepath + "status.txt"));
      line = "";
      String numTasks = "";
      while ((line = br.readLine()) != null) {
        if (line.startsWith("numTasks"))
          numTasks = line.split(" ")[2];

        if (line.startsWith("taskNum")) {
          bw.write("\t" + line.split(" ")[2] + "/" + numTasks);
          break;
        }
      }
    } catch (FileNotFoundException e) {
      bw.write("\tFNF");
    }

    br.close();
  }


  private static boolean getQuestionnaire(String id,
                                          String filename) throws Exception {
    Log.println("\ngetQuestionnaire", filename, id);

    String file_in = path_sessions + id + "/" + filename;
    Log.println(file_in);

    try {
      BufferedReader br = new BufferedReader(new FileReader(file_in));
      String line = "";
      while ((line = br.readLine()) != null) {
        Log.println(line);
        if (line.startsWith("Q")) {
          String response = line.split("\t")[1];
          response = response.substring(1, response.length() - 1); // starts and ends w/ '-'
          bw.write("\t" + response);
        }
      }
      br.close();

    } catch (FileNotFoundException e) {
      Log.println("No demographics file. Skipping.");
      bw.write("\t-1");
      return false;
    }
    return true;
  }


  private static void getTaskEvents(String id) throws Exception {
    Log.println("\ngetTaskEvents", id);

    RandomAccessFile raf = new RandomAccessFile(path_sessions + id + "/" + file_events, "r");

    String line = raf.readLine();
    Log.println(line);

    StringBuilder sb_blob = new StringBuilder();
    long start_time = 0;
    long end_time = 0;
    long last_time = 0;
    short time_incr = 0;

    // Date and time
    String date = get_date(line);
    long time = get_millis(line);
    Log.prbln("start", date, time);
    bw.write("\t" + date + "\t" + time);


    while ((line = raf.readLine()) != null) {
      line = clean_line(line);
      Log.println("\n> " + line);


      // Append next line if no timestamp: part of multi-line answer
      long pos = raf.getFilePointer();
      String next_line = raf.readLine();

      while (next_line != null && !next_line.startsWith("YMD")) {
        line = line + "][" + next_line;
        pos = raf.getFilePointer();
        next_line = raf.readLine();
      }
      raf.seek(pos);


      // TIMESTAMP
      time = get_millis(line);
      // Fix collision of two events within 1 second: add 10 ms
      if (time == last_time)
        time_incr += 10;
      else
        time_incr = 0;
      last_time = time;


      // Time of task-instructions start, UE survey start, session end
      if (line.contains("task_instructions.jsp") || //
          line.contains("User_Experience") || //
          line.contains("Thanks_ubc.jsp")) {
        bw.write("\t" + time);
        continue;
      }

      // Is last line
      if (next_line == null)
        break;


      // Start of task
      String[] bits = line.split("\\.txt "); // task file
      if (bits.length > 1) {
        String condition = bits[0].substring(bits[0].lastIndexOf(' ')).trim();
        Log.prbln("condition", condition);
        bw.write("\t" + condition);
        //        bw.write("\n\n" + condition); // test only

      } else {
        Log.println("line [" + line + "]");

        // ACTION PARAMETERS
        bits = line.split(" +");
        TreeMap<String, String> params = new TreeMap<String, String>();
        for (String bit : bits) {
          String[] key_val = bit.split("=", 2);
          if (key_val.length > 1) {
            allParams.add(key_val[0]);
            params.put(key_val[0], key_val[1]);
            Log.println(" ", key_val[0], "=", key_val[1]);
          }
        }
        Log.println("params " + params);

        if (params.isEmpty())
          continue;

        // ACTIONS
        String action = params.get(p_action);
        Log.prbln("action", action);

        if (action == null)
          continue;

        String action_spc = " " + action + " ";

        if (" start task_start ".contains(action_spc))
          start_time = time;


        // BLOB

        // END OF TASK -- incl. user answer
        else if (" task_done client_done ".contains(action_spc)) {

          // write start, end, answer, blob
          end_time = time;

          // If blob ends with comma
          if (sb_blob.length() > 2 && sb_blob.charAt(sb_blob.length() - 2) == ',') // -2: ends with ', '
            sb_blob.setLength(sb_blob.length() - 2); // rm trailing ','

          String ease_of_use = params.get("ease");
          if (ease_of_use == null)
            ease_of_use = params.get("easeofuse");

          String satisfaction = params.get("sat");
          if (satisfaction == null)
            satisfaction = params.get("satisfaction");

          bw.write( //
              "\t" + start_time //
                  + "\t" + end_time //
                  + "\t" + params.get("answer") //
                  + "\t" + ease_of_use //
                  + "\t" + satisfaction //
                  + "\t" + params.get("comment") //
                  + "\t{ " + sb_blob.toString() + " }");
          bw.flush();

          // reset blob for next task
          sb_blob = new StringBuilder();
          start_time = 0;
          last_time = 0;
          time_incr = 0;

          continue;
        }


        // Ignored actions
        else if (" done unload ".contains(action_spc))
          continue;


        // CONTENT task
        else if ("content_search".equals(action)) {
          sb_blob.append("\"" + (time + time_incr) + "\": {" //
              + "\"content_search\": \"" + params.get("query") + "\"}, ");
        }

        else if ("content_request_book".equals(action)) {
          sb_blob.append("\"" + (time + time_incr) + "\": {" //
              + "\"content_request_book\": {" //
              + "\"book\": " + params.get("bookID") //
              + ", \"snippet\": \"" + params.get("snippet") + "\"" //   
              + "}}, ");
        }

        else if ("content_request_page".equals(action)) {
          sb_blob.append("\"" + (time + time_incr) + "\": {" //
              + "\"content_request_page\": \"" + params.get("bookPage") + "\"}, ");
        }

        else if ("content_page_list".equals(action)) {
          sb_blob.append("\"" + (time + time_incr) + "\": {" //
              + "\"content_page_list\": \"" + params.get("bookPage") + "\"}, ");
        }

        else if ("content_page_prev".equals(action)) {
          sb_blob.append("\"" + (time + time_incr) + "\": {" //
              + "\"content_page_prev\": \"" + params.get("bookPage") + "\"}, ");
        }

        else if ("content_page_next".equals(action)) {
          sb_blob.append("\"" + (time + time_incr) + "\": {" //
              + "\"content_page_next\": \"" + params.get("bookPage") + "\"}, ");
        }


        // META task
        else if ("meta_search".equals(action)) {
          sb_blob.append("\"" + (time + time_incr) + "\": {" //
              + "\"meta_search\": \"" + params.get("query") + "\"" //
              + ", \"search\": \"" + params.get("search").substring(2) + "\"" //
              + ", \"terms\": \"" + params.get("terms").substring(2) + "\"" //
              + "}, ");
        }

        else if ("meta_request_entry".equals(action)) { //
          sb_blob.append("\"" + (time + time_incr) + "\": {" //
              + "\"meta_request_entry\": \"" + params.get("entryName") + "\"}, ");
        }

        else if ("show_entries".equals(action)) { //
          sb_blob.append("\"" + (time + time_incr) + "\": {" //
              + "\"show_entries\": \"" + params.get("visible") + "\"}, ");
        }

        else if ("meta_request_book".equals(action)) { //
          sb_blob.append("\"" + (time + time_incr) + "\": {" //
              + "\"meta_request_book\": {" //
              + "\"book\":" + params.get("bookID") //
              + ", \"pg\":\"" + params.get("bookPage") + "\""//
              + "}}, ");
        }

        else if ("meta_request_page".equals(action)) {
          sb_blob.append("\"" + (time + time_incr) + "\": {" //
              + "\"meta_request_page\": \"" + params.get("bookPage") + "\"}, ");
        }

        else if ("meta_page_prev".equals(action)) {
          sb_blob.append("\"" + (time + time_incr) + "\": {" //
              + "\"meta_page_prev\": \"" + params.get("bookPage") + "\"}, ");
        }

        else if ("meta_page_next".equals(action)) {
          sb_blob.append("\"" + (time + time_incr) + "\": {" //
              + "\"meta_page_next\": \"" + params.get("bookPage") + "\"}, ");
        }

        else
          Log.exit("WARN: unknown action [" + action + "]");
        // TODO -- good for catching forgotten parameters

      }
    }
    Log.println("getTaskEvents done");

    raf.close();
  }


  private static void init_bw() throws Exception {
    String path = Locals.path_data + Locals.dir_results;
    Log.println(path);
    File f = new File(path);
    f.mkdirs();

    bw = new BufferedWriter(new FileWriter(path + Locals.file_csv_raw));
  }


  /**
   * arg[0] is the host IP <BR>
   * arg[1] is the port
   * 
   * @param args
   */
  public static void main(String[] args) throws Exception {
    Locals.init();
    Util.init(Locals.file_log_raw);
    Log.println("\nmain");

    path_sessions = Locals.path_sessions;

    Log.println("\nPARSING SESSION DATA\n" + path_sessions + "\n");

    init_bw();
    write_header();
    //    main_test();
    process_results_dir();
    bw.close();

    Log.println("\n   --- done ---");
  }


  protected static void main_test() {
    //    String id = "160522135226692";
    String id = "160614182532107";

    process_user(id);
  }


  protected static void process_results_dir() {
    try {
      // Process user result dirs
      File file = new File(path_sessions);
      File[] user_dirs = file.listFiles();

      Log.prbln("user dirs", user_dirs);
      Log.println();

      for (File dir : user_dirs) {
        if (!dir.isDirectory())
          continue;

        String id = dir.getName();
        Log.println("\ndir", id);

        // skip if not in id format: all digits
        try {
          Long.parseLong(id);
          process_user(id);

        } catch (Exception e) {
          Log.println("skip ", id);
        }
      }

    } catch (Exception e) {
      Log.println(e);
    }
  }


  private static void process_user(String id) {
    Log.println("\n\nuser", id);

    try {
      bw.write(id);

      getQuestionnaire(id, file_q_demographic);
      getQuestionnaire(id, file_q_userExperience);

      getTaskEvents(id);

      bw.write("\n"); // one line per user

      // TEST ONLY: Show all parameters collected by the task client
      System.out.println("\n** Task parameters:");
      Iterator<String> it_params = allParams.iterator();
      while (it_params.hasNext())
        Log.println(it_params.next());

    } catch (Exception e) {
      Log.println(e);
    }
  }


  private static long toMillis(String hms) {
    return TimeUtils.hms_to_seconds(hms) * 1000;
  }


  private static void write_header() throws Exception {
    BufferedReader br = new BufferedReader(new FileReader(Locals.path_data + Locals.file_header_raw));

    bw.write("userID");

    String line = "";
    while ((line = br.readLine()) != null) {
      line = line.trim();
      if (line.isEmpty())
        continue;
      bw.write("\t" + line);
    }
    bw.write("\n");

    br.close();
  }

}
