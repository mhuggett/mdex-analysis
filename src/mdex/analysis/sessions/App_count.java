package mdex.analysis.sessions;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

import mdex.analysis.Locals;
import mdex.util.Log;


/**
 * Counts all conditions present in the specified results folder, to ensure that all conditions have been equally
 * pursued. Flags errors such as missing task results, condition line, missing files.
 *
 * @author mhuggett
 */

public class App_count {

  static TreeMap<String, Integer> condition_counts = new TreeMap<>();
  static TreeSet<String> dirs_no_task_results = new TreeSet<>();
  static TreeSet<String> dirs_no_condition_line = new TreeSet<>();
  static TreeSet<String> dirs_not_complete = new TreeSet<>();


  public static void main(String[] args) throws Exception {
    new Log("logs/log_counts.txt");
    Log.println("\nmain");

    Locals.init();

    Log.println("\n[" + Locals.path_sessions + "]\n");

    count_conditions();

    write_results();

    Log.println("\n   --- done ---\n");
  }


  public static void count_conditions() throws IOException, FileNotFoundException {
    File[] dirs = new File(Locals.path_sessions).listFiles(File::isDirectory);
    Log.println("Number of dirs: " + dirs.length + "\n");

    for (File dir : dirs) {
      Log.println("dir " + dir.getName());

      File[] files = dir.listFiles();
      boolean isFileFound = false;

      for (File file : files) {
        //        Log.println(" ", file.getName());

        if (file.getName().startsWith("task_results")) {
          isFileFound = true;
          int num_tasks = 0;
          int num_tasks_done = 0;

          String condition = null;
          Integer condition_count = null;

          // Scan the file
          try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = br.readLine()) != null) {
              if (line.startsWith("taskSet_id:")) {
                condition = line.split(" *: *")[1];
                Log.println("    condition " + condition);
                condition_count = condition_counts.get(condition);
                if (condition_count == null)
                  condition_count = 0;

              } else if (line.startsWith("taskSet:")) {
                num_tasks = line.split(",").length;
                //                Log.println("    num_tasks " + num_tasks);

              } else if (line.startsWith("task:")) {
                ++num_tasks_done;
              }
            }
          }

          // Update findings
          if (condition == null) {
            Log.println("    WARN: task_results: no task_id");
            dirs_no_condition_line.add(dir.getName());

          } else if (num_tasks != num_tasks_done) {
            Log.println("    WARN: tasks not complete:  " + num_tasks_done + " < " + num_tasks);
            dirs_not_complete.add(dir.getName() + "   " + num_tasks_done + " < " + num_tasks);

          } else // should be okay
            condition_counts.put(condition, condition_count + 1);
        }
      }
      if (!isFileFound) {
        Log.println("    FATAL: no task_results file");
        dirs_no_task_results.add(dir.getName());
      }
    }
  }


  public static void write_results() {
    //    Log.println(t_counts);

    Log.println("\n\nDirs w/ no task_results file (to be discarded):");
    for (String dir : dirs_no_task_results)
      Log.println(" " + dir);

    Log.println("\nDirs w/ no condition line (to be reviewed):");
    for (String dir : dirs_no_condition_line)
      Log.println(" " + dir);

    Log.println("\nDirs w/ incomplete tasks (review/discard):");
    for (String dir : dirs_not_complete)
      Log.println(" " + dir);

    Log.println("\nCondition counts (complete only):");
    for (Map.Entry<String, Integer> entry : condition_counts.entrySet()) {
      Log.println(" " + entry.getKey() + " " + entry.getValue());
    }
  }

}
