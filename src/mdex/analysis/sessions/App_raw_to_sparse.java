package mdex.analysis.sessions;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.TreeMap;

import mdex.analysis.Locals;
import mdex.util.Log;

public class App_raw_to_sparse {

  private static ArrayList<String> al_runtime_errors;
  private static ArrayList<String> al_task_order;
  private static BufferedWriter bw;
  private static int num_survey_Qs;


  public static void main(String[] args) throws Exception {
    Locals.init();
    Util.init(Locals.file_log_sparse);
    Log.println("\nmain");

    Log.println("\nPARSING SESSION DATA\n" + Locals.path_sessions + "\n");

    al_runtime_errors = new ArrayList<>();
    bw = new BufferedWriter(new FileWriter(Locals.path_results + Locals.file_csv_sparse));

    write_header();

    try {
      BufferedReader br = new BufferedReader(new FileReader(Locals.path_results + Locals.file_csv_raw));

      String line = br.readLine(); // skip header
      while ((line = br.readLine()) != null)
        process_session(line);

      br.close();
      bw.close();

    } catch (Exception e) {
      Log.println(e);
    }

    Log.println("\nRuntime Errors:");
    for (String e : al_runtime_errors)
      Log.println(e);

    Log.println("\n   --- done ---");
  }


  private static void process_session(String line) throws Exception {
    String[] bits = line.split("\t");

    try {
      // Accumulate row of sparse table
      StringBuilder sb = new StringBuilder();

      sb.append(bits[0]); // userID

      int i = 0;

      // Write survey answers
      while (i < num_survey_Qs)
        sb.append("\t" + bits[++i]);

      Log.println();

      // Date, demo-survey start, instructions start
      sb.append("\t" + bits[++i]);
      sb.append("\t" + bits[++i]);
      sb.append("\t" + bits[++i]);

      // Store user's task sets, sorted
      Log.println("**User " + bits[0]);
      TreeMap<String, String[]> tm = new TreeMap<String, String[]>();
      for (int c = 0; c < 6; c++) { // 6: number of tasks per user
        String[] vals = new String[7]; // 7: number of (non-id) columns per task 

        String task = bits[++i]; // task_id: not stored
        vals[0] = bits[++i]; // start
        vals[1] = bits[++i]; // end
        vals[2] = bits[++i]; // answer
        vals[3] = bits[++i]; // ease
        vals[4] = bits[++i]; // satisfaction
        vals[5] = bits[++i]; // comment
        vals[6] = bits[++i]; // events

        tm.put(task, vals);

        Log.println(task);
      }

      // Write user's tasks into sparse table line
      Log.println("**To sparse:");
      for (String task : al_task_order) {
        String[] vals = tm.get(task);

        if (vals == null) {
          Log.println(task);
          sb.append("\t\t\t\t\t\t\t"); // Was not assigned this task, skip 7 columns

        } else {
          Log.prbln(task + " BING");
          for (String val : vals)
            sb.append("\t" + val);
        }
      }

      // UE-survey start, session end
      sb.append("\t" + bits[++i]);
      sb.append("\t" + bits[++i]);

      // Print user row to sparse table
      sb.append("\n");
      bw.write(sb.toString());
      bw.flush();


    } catch (ArrayIndexOutOfBoundsException e) {
      // Skip to next line
      String msg = "WARN: bad input line for user " + bits[0] + ".  Skipping.";
      Log.println("\n" + msg);
      al_runtime_errors.add(msg);
    }
  }


  /**
   * Writes the specified header to the output CSV file. <br>
   * Counts the number of survey questions. <br>
   * Stores the order of the task_IDs as they appear in the header config.
   * 
   * @param bw
   * @param file_header
   * @return
   * @throws Exception
   */
  private static void write_header() throws Exception {
    BufferedReader br = new BufferedReader(new FileReader(Locals.path_data + Locals.file_header_sparse));

    bw.write("userID");

    num_survey_Qs = 0;
    al_task_order = new ArrayList<String>();
    String last_task = "";

    String line = "";
    while ((line = br.readLine()) != null) {
      line = line.trim();
      if (line.isEmpty() || line.charAt(0) == '#') // comment
        continue;
      bw.write("\t" + line);

      if (line.charAt(0) == 'Q')
        ++num_survey_Qs;

      if (line.startsWith("content_") || line.startsWith("mui_")) {
        String task = line.substring(0, line.lastIndexOf('_'));
        if (!task.equals(last_task)) {
          al_task_order.add(task);
          last_task = task;
        }
      }
    }
    Log.println("Num Survey Questions:", num_survey_Qs);
    Log.println("Task Order:\n", al_task_order);

    bw.write("\n");

    br.close();
  }
}
