package mdex.analysis;

import mdex.util.Config;
import mdex.util.Log;

public class Locals {

  public static final String charset = "UTF-8";

  public static String dir_results;

  public static final String file_demographic = "Demographic.txt";

  public static final String path_data = "data/";

  public static String path_sessions;

  /** Apple folder settings config */
  public static final String stupid_garbage = ".DS_Store";

  public static String file_header_raw;
  public static String file_header_sparse;

  public static String file_csv_raw;
  public static String file_csv_sparse;

  public static String file_log_raw;
  public static String file_log_sparse;

  public static String dir_logs;

  public static String path_results;


  public static void init() throws Exception {
    Log.println("Locals init MDEX_ANALYSIS");

    Config.load(path_data + "config.txt");

    path_sessions = Config.get("path_sessions");
    if (Locals.path_sessions == null //
        || Locals.path_sessions.isEmpty()) {
      Log.println("\nFATAL: Need path to session data.\n");
      Log.exit();
    }
    if (!path_sessions.endsWith("/"))
      path_sessions += "/";

    dir_logs = Config.get("dir_logs");
    dir_results = Config.get("dir_results");

    path_results = path_data + dir_results;

    file_header_raw = Config.get("file_header_raw");
    file_header_sparse = Config.get("file_header_sparse");

    file_log_raw = Config.get("file_log_raw");
    file_log_sparse = Config.get("file_log_sparse");

    file_csv_raw = Config.get("file_csv_raw");
    file_csv_sparse = Config.get("file_csv_sparse");
  }

}
