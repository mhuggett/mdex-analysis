package mdex.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;


/**
 * Intended to save all or some Sys.out.prints to file, since there can be quite a lot, and it's nice to have it in a
 * txt file to peruse at leisure...
 * <p>
 * The class is self-contained; thus the default output file name is set *here*, but can be over-ridden using the
 * parameterized constructor.
 *
 * @author mike
 */

public class Log {

  /** Carriage return / line feed character. Default <b>\n</b> to console; set to <b>&lt;br&gt;</b> for web pages. */
  public static String CRLF = "\n";

  private static File file;

  private static boolean isMuted;

  /** If true, adds a timestamp at the start of each line. Default is 'false'. */
  private static boolean isTimestamped = false; // default

  /** If false, mute output to the console (but continue to append to log). Default is 'true'. */
  private static boolean isToConsole = true; // default

  /** The Writer used by the log. */
  private static BufferedWriter out;


  /**
   * Default constructor; used for development, as the output folder is constant and known in advance.
   *
   * @throws IOException
   */
  Log() throws IOException {
    init("_log.txt", false); // default in user.dir
  }


  /**
   * Production constructor; puts any generated logs in the work folder.
   *
   * @param filepath
   *          The fully qualified path to the log file.
   * @throws IOException
   */
  public Log(String filepath) throws IOException {
    init(filepath, false);
  }


  /**
   * Create a new Log object.
   *
   * @param filepath
   *          The destination file
   * @param isAppend
   *          Whether each run appends to today's existing log, or deletes today's log and creates a new one.
   * @throws IOException
   */
  public Log(String filepath, boolean isAppend) throws IOException {
    init(filepath, isAppend);
  }


  public static void arrayln(String[] array) {
    int i = 0;
    for (String element : array)
      println(++i, ":", element);
  }


  /**
   * Close the log and exit.
   */
  public static void exit() {
    shutdown();
    System.exit(0);
  }


  /**
   * Close the log and exit.
   */
  public static void exit(Exception e) {
    println(e);
    exit();
  }


  /**
   * Prints a final message, closes the log, and exits.
   *
   * @param message
   *          Final log message before shutdown.
   */
  public static void exit(Object message) {
    isMuted = false; // Always show exit
    println("\n" + String.valueOf(message));
    exit();
  }


  public static void exit(Object... ss) {
    println("", ss);
    exit();
  }


  public static void flush() {
    try {
      out.flush();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }


  public static String getFilePath() {
    try {
      return file.getCanonicalPath();

    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }


  private static void init(String filepath,
                           boolean isAppend) throws IOException {
    if (isToConsole)
      System.out.println("Log::init " + filepath);
    file = new File(filepath);

    if (!isAppend)
      file.delete();

    // file.getParentFile() unexpectedly returns null:
    file.getAbsoluteFile().getParentFile().mkdirs();

    out = new BufferedWriter(new FileWriter(file.getCanonicalPath(), isAppend));

    if (file.length() > 0)
      println();

    // Print log location
    prbln("LOG " + (isAppend ? "append" : "new"), file.getCanonicalPath());
    printTime();
    println();
  }


  public static void main(String[] args) {
    Log.prb("one", "two", "three");
    String s = "one";
    Log.println("that", s);
  }


  public static void muteOff() {
    isMuted = false;
  }


  public static void muteOn() {
    isMuted = true;
  }


  /**
   * Pretty-prints a TreeMap, one element per line.
   *
   * @param tm
   */
  public static void ppr(TreeMap<String, String> tm) {
    for (Map.Entry<String, String> entry : tm.entrySet())
      println(entry.getKey(), "=", entry.getValue());
  }


  /**
   * Prints a String array in form: <BR>
   * [a] [b] [c] ...
   *
   * @param ss
   */
  public static void prb(Object... ss) {
    if (isMuted)
      return;

    StringBuilder sb = new StringBuilder();
    if (ss.length == 0)
      sb.append("[]");
    else
      for (Object s : ss) {
        sb.append("[");
        sb.append(String.valueOf(s));
        sb.append("] ");
      }

    String s = sb.toString();
    if (isToConsole)
      System.out.println(s);
    writeToLog(s);
    writeToLog("\n");
    try {
      out.flush();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }


  // public static void prb(Vector<Object> vec) {
  // if (isMuted)
  // return;
  //
  // StringBuilder sb = new StringBuilder();
  // if (vec.size() == 0)
  // sb.append("[]");
  // else
  // for (Object s : vec) {
  // sb.append("[");
  // sb.append(String.valueOf(s));
  // sb.append("] ");
  // }
  //
  // String s = sb.toString();
  // if (isToConsole)
  // System.out.println(s);
  // writeToLog(s);
  // writeToLog("\n");
  // try {
  // out.flush();
  // } catch (Exception e) {
  // e.printStackTrace();
  // }
  // }


  /**
   * Prints a String array in form: <BR>
   * [a] [b] [c] ...
   *
   * @param ss
   */
  public static void prb(String... ss) {
    prb((Object[]) ss);
  }


  /**
   * Prints a series of comma-delimited Strings (or a String array) in form: <BR>
   * a [b] [c] ...<BR>
   *
   * @param ss
   *          A series of Strings, or a String array.
   */
  public static void prbln(Object... ss) {
    if (isMuted)
      return;

    StringBuffer sb = new StringBuffer(isTimestamped ? TimeUtils.getYmdHms() + " " : "");

    // sb.append("\n");
    sb.append(String.valueOf(ss[0]));

    for (int i = 1; i < ss.length; i++) {
      sb.append(" [");
      sb.append(String.valueOf(ss[i]));
      sb.append("]");
    }

    String s = sb.toString();
    if (isToConsole)
      System.out.println(s);
    writeToLog(s);
    writeToLog("\n");
    try {
      out.flush();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }


  /**
   * Prints a label, followed by the contents of a String array.
   *
   * @param label
   *          A descriptive label.
   * @param array
   *          An array of String.
   */
  public static void prbln(Object label,
                           Object[] array) {
    print(isTimestamped ? TimeUtils.getYmdHms() : "" + label + " ");
    if (array == null)
      println("null");
    else
      prb(array);
  }


  /**
   * Prints a label, followed by the contents of a String array.
   *
   * @param label
   *          A descriptive label.
   * @param vec
   *          An array of String.
   */
  public static void prbln(Object label,
                           Vector<Object> vec) {
    print(isTimestamped ? TimeUtils.getYmdHms() : "" + label + " ");
    if (vec == null)
      println("null");
    else
      prb(vec);
  }


  public static void print(Object o) {
    if (isMuted)
      return;

    String s = String.valueOf(o);
    if (isToConsole)
      System.out.print(s);
    writeToLog(s);
  }


  /**
   * Prints an array, with a delimiter between elements.
   *
   * @param delimiter
   *          The delimiter to put between elements.
   * @param ss
   *          The array.
   */
  public static void print(String delimiter,
                           Object[] ss) {
    if (isMuted)
      return;

    StringBuffer sb = new StringBuffer();
    sb.append(String.valueOf(ss[0]));

    for (int i = 1; i < ss.length; i++) {
      sb.append(" " + (delimiter.isEmpty() ? "" : delimiter + " "));
      sb.append(String.valueOf(ss[i]));
    }

    String s = sb.toString();
    if (isToConsole)
      System.out.print(s);
    writeToLog(s);
    try {
      out.flush();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }


  private static void printTime() {
    String local = TimeUtils.getTime_Server_Local();
    println(local);
    String utc = TimeUtils.getUTC_time_server();
    if (!utc.equals(local))
      println(utc);
  }


  public static void println() {
    println("");
  }


  public static void println(Exception e) {
    println(e.toString());
    StackTraceElement[] stack = e.getStackTrace();
    String spc = "        at ";
    for (StackTraceElement s : stack) {
      println_noTS(spc + s.toString());
    }
  }


  public static String println(Object o) {
    if (isMuted)
      return "";

    String s = String.valueOf(o);

    StringBuilder sb = new StringBuilder();

    if (isTimestamped && !s.isEmpty()) {
      // Strip and re-add any leading \n
      int ptr = 0;
      while (s.charAt(ptr) == '\n') {
        sb.append('\n');
        ptr++;
      }
      sb.append(TimeUtils.getYmdHms());
      sb.append(" ");
      sb.append(s.substring(ptr));

    } else
      sb.append(s);

    s = sb.toString();

    writeToLog(s);
    writeToLog("\n");
    if (isToConsole)
      System.out.println(s);
    try {
      out.flush();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return s + "\n";
  }


  public static String println(Object... ss) {
    return println("", ss);
  }


  /**
   * Prints an array, with a delimiter between elements.
   *
   * @param delimiter
   *          The delimiter to put between elements.
   * @param ss
   *          The array.
   */
  public static String println(String delimiter,
                               Object[] ss) {
    if (isMuted)
      return "";

    StringBuffer sb = new StringBuffer();
    sb.append(String.valueOf(ss[0]));

    for (int i = 1; i < ss.length; i++) {
      sb.append(" " + (delimiter.isEmpty() ? "" : delimiter + " "));
      sb.append(String.valueOf(ss[i]));
    }

    String s = sb.toString();
    if (isToConsole)
      System.out.println(s);
    writeToLog(s);
    writeToLog("\n");
    try {
      out.flush();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return sb.append("\n").toString();
  }


  public static String println_noTS(Object o) {
    if (isMuted)
      return "";

    String s = String.valueOf(o);

    writeToLog(s);
    writeToLog("\n");
    if (isToConsole)
      System.out.println(s);
    try {
      out.flush();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return s + "\n";
  }


  /**
   * Q: For web pages, why not just output regular text surrounded by a PRE tag?
   *
   * @param CRLF
   *          A String for the carriage return / line feed character. Default '\n' for standard-out; set to '&lt;BR&gt;'
   *          for web pages.
   */
  public static void setCRLF(String CRLF) {
    Log.CRLF = CRLF;
  }


  public static void setConsoleOut(boolean tf) {
    isToConsole = tf;
  }


  public static void setLogFile(String filepath) {
    try {
      new Log(filepath);

    } catch (IOException e) {
      Log.println(e);
    }
  }


  public static void setMute(boolean isMuted) {
    Log.isMuted = isMuted;
  }


  public static void setTimestamp(boolean tf) {
    isTimestamped = tf;
  }


  public static void shutdown() {
    println();
    println("Log shutdown at -");
    printTime();

    try {
      out.close();

    } catch (Exception e) {
      e.printStackTrace();
    }
  }


  // Not used:
  public static String wrap(Object str) {
    return new StringBuffer().append("[").append(String.valueOf(str)).append("]").toString();
  }


  /**
   * Writes a single character.
   *
   * @param x
   *          int specifying a character to be written.
   */
  public static void write(int x) {
    try {
      out.write(x);

    } catch (Exception e) {
      e.printStackTrace();
    }
  }


  private static void writeToLog(String str) {
    try {
      if (out == null)
        new Log();

      out.write(str);
      // GUI.showOutput(str);

    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
