package mdex.util;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.TreeMap;

public class TimeUtils {

  private static final DecimalFormat df = new DecimalFormat("00");
  private static final DecimalFormat df3 = new DecimalFormat("000");

  private static String[] months = { "January", "February", "March", "April", "May", "June", "July", "August",
      "September", "October", "November", "December" };

  private static TreeMap<String, String> tm_month_num;

  public static final int UNIT_DAY = 0;
  public static final int UNIT_WEEK = 1;
  public static final int UNIT_YEAR = 2;


  /**
   * Gets the <b>local</b> time within the given date in format <b>HH:mm:ss.SSS</b>. Year, month, and day are ignored.
   *
   * @return
   */
  public static String date_to_hms_ms(Date date) {
    return dateZone_to_hms_ms(date, null);
  }


  /**
   * Gets the time within the given date in UTC zone in format <b>HH:mm:ss.SSS</b>. Year, month, and day are ignored.
   *
   * @return
   */
  public static String date_to_hms_ms_UTC(Date date) {
    return dateZone_to_hms_ms(date, "UTC");
  }


  /**
   * Gets the time of the given Date in format <b>HH:mm:ss.SSS</b> for the given time zone. Year, month, and day are
   * ignored.
   *
   * @return
   */
  public static String dateZone_to_hms_ms(Date date,
                                          String zone) {
    DateFormat df = new SimpleDateFormat("HH:mm:ss.SSS");
    if (!(zone == null || zone.isEmpty()))
      df.setTimeZone(TimeZone.getTimeZone(zone));
    return df.format(date);
  }


  public static String getDateISO() {
    Calendar now = Calendar.getInstance();
    int yyyy = now.get(Calendar.YEAR);
    String mm = df.format(now.get(Calendar.MONTH) + 1); // months 0-11 +1
    String dd = df.format(now.get(Calendar.DAY_OF_MONTH));
    return yyyy + "-" + mm + "-" + dd;
  }


  // date string in format yymmdd
  public static String getDateISO(Calendar calendar) {
    return new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
  }


  // date string in format yymmdd
  public static String getDateISO(Date date) {
    return new SimpleDateFormat("yyyy-MM-dd").format(date);
  }


  /**
   * For use with timestamps that need to be standard/daylight time agnostic, guarantees correct elapsed times between
   * timestamps. Format is <b>yyyy-MM-dd</b>
   *
   * @return
   */
  public static String getDateISO_UTC() {
    DateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd");
    TimeZone utcTime = TimeZone.getTimeZone("UTC");
    timeFormat.setTimeZone(utcTime);
    return timeFormat.format(new Date());
  }


  /**
   * For use with timestamps that need to be standard/daylight time agnostic, guarantees correct elapsed times between
   * timestamps. Format is <b>yyyy-MM-dd</b>
   *
   * @return
   */
  public static String getDateISO_UTC(Date date) {
    DateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd");
    TimeZone utcTime = TimeZone.getTimeZone("UTC");
    timeFormat.setTimeZone(utcTime);
    return timeFormat.format(date);
  }


  /**
   * Converts the current Calendar time to format <b>yy/mm/dd_hhmm</b>h <br/>
   * Does not include seconds or time zone.
   *
   * @param now
   *          The given Calendar time.
   * @return A String of the given time.
   */
  public static String getDateTime() {
    Calendar now = Calendar.getInstance();
    return getDateTime(now);
  }


  /**
   * Converts the given Calendar time to format <b>yy/mm/dd_hhmm</b>h <br/>
   * Does not include seconds or time zone.
   *
   * @param now
   *          The given Calendar time.
   * @return A String of the given time.
   */
  public static String getDateTime(Calendar now) {
    String yr = df.format(now.get(Calendar.YEAR));
    String mo = df.format(now.get(Calendar.MONTH) + 1);
    String dy = df.format(now.get(Calendar.DATE));

    String hh = df.format(now.get(Calendar.HOUR_OF_DAY));
    String mm = df.format(now.get(Calendar.MINUTE));
    return yr + "/" + mo + "/" + dy + "_" + hh + mm + "h";
  }


  /**
   * Gets the current local time in format <b>yy/mm/dd hhmm</b>h:<b>ss.iii</b>_<b>zzz</b>
   *
   * @return A String of the current local time.
   */
  public static String getDateTime_Millis_zone() {
    return getDateTime_Millis_zone(Calendar.getInstance());
  }


  /**
   * Gets the current local time in format <b>yy/mm/dd hhmm</b>h:<b>ss.iii</b>_<b>zzz</b>
   *
   * @return A String of the current local time.
   */
  public static String getDateTime_Millis_zone(Calendar now) {
    String ss = df.format(now.get(Calendar.SECOND));
    String ms = df3.format(now.get(Calendar.MILLISECOND));
    return getDateTime(now) + ":" + ss + "." + ms + "_" + getZoneOffset(now);
  }


  /**
   * Gets the current time in format yy/mm/dd <b>hh:mm:ss_zzz</b> <br/>
   * Time zone is expressed as UTC with offset in hours.
   *
   * @return A String of the given time.
   */
  public static Object getDateTime_Seconds_zone() {
    return getDateTime_Seconds_zone(Calendar.getInstance());
  }


  /**
   * Converts the given Calendar time to format yy/mm/dd <b>hh:mm:ss_zzz</b> <br/>
   * Time zone is expressed as UTC with offset in hours.
   *
   * @param now
   *          The given Calendar time.
   * @return A String of the given time.
   */
  public static String getDateTime_Seconds_zone(Calendar now) {
    // Calendar now = Calendar.getInstance();
    String yr = df.format(now.get(Calendar.YEAR));
    String mo = df.format(now.get(Calendar.MONTH) + 1);
    String dy = df.format(now.get(Calendar.DATE));

    String hh = df.format(now.get(Calendar.HOUR_OF_DAY));
    String mm = df.format(now.get(Calendar.MINUTE));
    String ss = df.format(now.get(Calendar.SECOND));
    return yr + "/" + mo + "/" + dy + "_" + hh + ":" + mm + ":" + ss + "_" + getZoneOffset(now);
  }


  /**
   * Gets the current local time in format - <br/>
   * <b>yy/mm/dd hhmm</b>h_<b>zzz</b> <br/>
   * <br/>
   * Time zone is expressed as UTC with offset in hours.
   *
   * @return A String of the current local time.
   */
  public static String getDateTime_zone() {
    Calendar now = Calendar.getInstance();
    return getDateTime(now) + "_" + getZoneOffset(now);
  }


  /**
   * Current date in format YYMMDD
   *
   * @return
   */
  public static String getDateYMD() {
    return getDateYMD(new Date());
  }


  /**
   * Date in format YYMMDD
   *
   * @return
   */
  public static String getDateYMD(Date date) {
    return new SimpleDateFormat("yyMMdd").format(date);
  }


  /**
   * Gets the day of the week for the given ISO date, represented as a full String, e.g. 'Monday'
   *
   * @param date_iso
   *          A String of a date in ISO format yyyy-MM-dd.
   * @return A String
   */
  public static String getDayOfWeek_full(String date_iso) {
    try {
      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
      Date date = sdf.parse(date_iso);
      SimpleDateFormat sdf_out = new SimpleDateFormat("EEEE");
      return sdf_out.format(date);

    } catch (Exception e) {
      Log.println(e);
    }
    return null;
  }


  /**
   * Gets the day of the week for the given ISO date, represented as a Calendar int, e.g. Calendar.MONDAY = 2
   *
   * @param date_iso
   *          A String of a date in ISO format yyyy-MM-dd.
   * @return An int
   */
  public static int getDayOfWeek_int(String date_iso) {
    try {
      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
      Date date = sdf.parse(date_iso);
      Calendar c = Calendar.getInstance();
      c.setTime(date);
      return c.get(Calendar.DAY_OF_WEEK);

    } catch (Exception e) {
      Log.println(e);
    }
    return 0;
  }


  /**
   * Calculates the number of days separating the given dates.
   *
   * @param dateISO_1
   * @param dateISO_2
   * @return An int
   */
  public static int getDaysBetween(String dateISO_1,
                                   String dateISO_2) {
    if (dateISO_1.isEmpty() || dateISO_2.isEmpty())
      return 0;
    String default_time = "00:00:00.000";
    long ms_1 = getMillis_epoch(dateISO_1, default_time);
    long ms_2 = getMillis_epoch(dateISO_2, default_time);
    long diff = Math.max(ms_1, ms_2) - Math.min(ms_1, ms_2);
    System.out.println(diff);
    // System.out.println(diff / 1000.0);
    // System.out.println((diff / 1000.0) / 3600);
    // System.out.println((diff / 1000.0) / 3600 / 24);
    return (int) Math.round((diff / 1000.0) / 3600 / 24);
  }


  /**
   * Gets the current time in millis.
   *
   * @return
   */
  public static long getMillis() {
    Calendar c = Calendar.getInstance();
    return c.getTimeInMillis();
  }


  /**
   * Gets millis since the epoch for the given <b>local</b> date and time.
   * <p>
   * For testing see <b>http://currentmillis.com/</b>
   *
   * @param date
   *          Date string in ISO format yyyy-MM-dd
   * @param time
   *          Time string in format hh:mm:ss.SSS
   * @return A long of elapsed millis.
   */
  public static long getMillis_epoch(String date,
                                     String time) {
    return getMillis_epoch(date, time, null);
  }


  /**
   * Gets millis since the epoch for the given date, time, and zone.
   * <p>
   * For testing see <b>http://currentmillis.com/</b>
   *
   * @param date
   *          Date string in ISO format yyyy-MM-dd
   * @param time
   *          Time string in format hh:mm:ss.SSS
   * @param zone
   *          A string describing the time zone. Defaults to local if null or empty.
   * @return A long of elapsed millis.
   */
  public static long getMillis_epoch(String date,
                                     String time,
                                     String zone) {
    String[] bits_date = date.split("-");
    String[] bits_time = time.split("[:\\.]");

    Calendar c;
    if (zone == null || zone.isEmpty())
      c = Calendar.getInstance();
    else
      c = Calendar.getInstance(TimeZone.getTimeZone(zone));
    c.clear();
    c.set(Integer.parseInt(bits_date[0]), // year
        Integer.parseInt(bits_date[1]) - 1, // month
        Integer.parseInt(bits_date[2]), // day
        Integer.parseInt(bits_time[0]), // hour
        Integer.parseInt(bits_time[1]), // minute
        Integer.parseInt(bits_time[2])); // second

    long millis = c.getTimeInMillis();
    // System.out.println(millis);

    if (bits_time.length > 3)
      return millis + Long.parseLong(bits_time[3]); // add utc millis

    return millis;
  }


  /**
   * For specified <b>local</b> date, gets from epoch to start of the specified time period (e.g. for day: 0000h, for
   * week: 0000h Monday, for year: 0000h Jan 1) in millis.
   * <p>
   * For testing see <b>http://currentmillis.com/</b>
   *
   * @param time_period
   *          Specified as TimeUtils.DAY, TimeUtils.WEEK, TimeUtils.YEAR
   * @param date
   *          Date string in ISO format yyyy-MM-dd
   * @param time
   * @return A long of elapsed millis.
   */
  public static long getMillis_epochToStartOf(int time_period,
                                              String date,
                                              String time) {
    return getMillis_epochToStartOf(time_period, date, time, null, Calendar.MONDAY);
  }


  public static long getMillis_epochToStartOf(int time_period,
                                              String date,
                                              String time,
                                              int calendar_day) {
    return getMillis_epochToStartOf(time_period, date, time, null, calendar_day);
  }


  /**
   * For specified date, time, and time-zone, gets from epoch to start of the specified time period (e.g. for day:
   * 0000h, for week: 0000h Monday, for year: 0000h Jan 1) in millis.
   * <p>
   * For testing see <b>http://currentmillis.com/</b>
   *
   * @param time_period
   *          Specified as TimeUtils.DAY, TimeUtils.WEEK, TimeUtils.YEAR
   * @param date
   *          Date string in ISO format yyyy-MM-dd
   * @param time
   *          Time string in format hh:mm:ss.SSS
   * @param zone
   *          String describing the date's time zone, e.g. "UTC". Defaults to local if null or empty.
   * @param calendar_day
   *          If time period Calendar.WEEK is specified, the day that marks the start of the week, e.g. Calendar.SUNDAY,
   *          or Calendar.MONDAY, etc.
   * @return A long of elapsed millis.
   */
  public static long getMillis_epochToStartOf(int time_period,
                                              String date,
                                              String time,
                                              String zone,
                                              int calendar_day) {
    String[] bits_date = date.split("-");

    Calendar c;
    if (zone == null || zone.isEmpty()) // local
      c = Calendar.getInstance();
    else
      c = Calendar.getInstance(TimeZone.getTimeZone(zone));
    c.clear();

    if (time_period == TimeUtils.UNIT_DAY) {
      c.set(Integer.parseInt(bits_date[0]), // year
          Integer.parseInt(bits_date[1]) - 1, // month
          Integer.parseInt(bits_date[2])); // day

    } else if (time_period == TimeUtils.UNIT_WEEK) {
      c.set(Integer.parseInt(bits_date[0]), // year
          Integer.parseInt(bits_date[1]) - 1, // month
          Integer.parseInt(bits_date[2])); // day
      c.getTime(); // update Calendar with changes
      int curr_day = c.get(Calendar.DAY_OF_WEEK);
      while (curr_day != calendar_day) { // Rewind to most recent calendar_day
        c.add(Calendar.DATE, -1);
        curr_day = c.get(Calendar.DAY_OF_WEEK);
      }
      // c.set(Calendar.DAY_OF_WEEK, calendar_day); // set to most recent N-day

    } else if (time_period == TimeUtils.UNIT_YEAR) {
      c.getTime(); // update Calendar with changes
      c.set(Calendar.YEAR, Integer.parseInt(bits_date[0]));
    }

    return c.getTimeInMillis();
  }


  /**
   * Returns epoch millis for the most recent day defined by the specified month and day.
   *
   * @param month_day
   *          String of month and day in format <b>MM-dd</b>
   * @return A long of millis since the epoch for this day
   */
  public static long getMillis_MonthDay(String month_day) {
    String[] bits = month_day.split("-");

    Calendar c;
    c = Calendar.getInstance();
    long ms_now = c.getTimeInMillis();
    int this_year = c.get(Calendar.YEAR);

    c.clear();
    c.set(this_year, // year
        Integer.parseInt(bits[0]) - 1, // month
        Integer.parseInt(bits[1])); // day

    long ms_test = c.getTimeInMillis();

    // Most recent is last year, not this
    if (ms_test > ms_now) {
      c.add(Calendar.YEAR, -1);
      ms_test = c.getTimeInMillis();
    }
    return ms_test;
  }


  /**
   * Current local date and time in standard 'server' format:<BR>
   * EEE MMM dd HH:mm:ss z yyyy
   *
   * @return A String of the current local time.
   */
  public static String getTime_Server_Local() {
    Date date = new Date();
    return String.valueOf(date);
  }


  /**
   * Converts the given Calendar time to format <b>hhmm</b>h_<b>yy/mm/dd</b> <br/>
   * Does not include seconds or time zone.
   *
   * @param now
   *          The given Calendar time.
   * @return A String of the given time.
   */
  public static String getTimeDate(Calendar now) {
    // Calendar now = Calendar.getInstance();
    String yr = df.format(now.get(Calendar.YEAR));
    String mo = df.format(now.get(Calendar.MONTH) + 1);
    String dy = df.format(now.get(Calendar.DATE));

    String hh = df.format(now.get(Calendar.HOUR_OF_DAY));
    String mm = df.format(now.get(Calendar.MINUTE));
    return hh + mm + "h_" + yr + "/" + mo + "/" + dy;
  }


  /**
   * Gets the current local time in format - <br/>
   * <b>hhmm</b>h_<b>yy/mm/dd_zzz</b> <br/>
   * <br/>
   * Time zone is expressed as UTC with offset in hours.
   *
   * @return A String of the current local time.
   */
  public static String getTimeDate_zone() {
    Calendar now = Calendar.getInstance();
    return getTimeDate(now) + "_" + getZoneOffset(now);
  }


  // date string in format yymmdd
  public static String getUTC_dateISO(Date date) {
    DateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd");
    timeFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
    return timeFormat.format(date);
  }


  /**
   * For use with timestamps that need to be standard/daylight time agnostic, guarantees correct elapsed times between
   * timestamps. Format is <b>HH:mm:ss.SSS</b>
   *
   * @return
   */
  public static String getUTC_hms_ms() {
    Date date = new Date();
    return date_to_hms_ms_UTC(date);
  }


  /**
   * Gets millis since the epoch for the given <b>UTC</b> date and time.
   * <p>
   * For testing see <b>http://currentmillis.com/</b>
   *
   * @param date
   *          Date string in ISO format yyyy-MM-dd
   * @param time
   *          Time string in format hh:mm:ss.SSS
   * @return A long of elapsed millis.
   */
  public static long getUTC_millis_epoch(String date,
                                         String time) {
    return getMillis_epoch(date, time, "UTC");
  }


  /**
   * For specified <b>UTC</b> date, gets from epoch to start of the specified time period (e.g. for day: 0000h, for
   * week: 0000h Monday, for year: 0000h Jan 1) in millis.
   * <p>
   * For testing see <b>http://currentmillis.com/</b>
   *
   * @param time_period
   *          Specified as TimeUtils.DAY, TimeUtils.WEEK, TimeUtils.YEAR
   * @param date
   *          Date string in ISO format yyyy-MM-dd
   * @param time
   * @return A long of elapsed millis.
   */
  public static long getUTC_millis_epochToStartOf(int time_period,
                                                  String date,
                                                  String time) {
    return getMillis_epochToStartOf(time_period, date, time, "UTC", Calendar.MONDAY);
  }


  public static long getUTC_millis_epochToStartOf(int time_period,
                                                  String date,
                                                  String time,
                                                  int calendar_day) {
    return getMillis_epochToStartOf(time_period, date, time, "UTC", calendar_day);
  }


  /**
   * Current UTC date and time in standard 'server' format:<BR>
   * EEE MMM dd HH:mm:ss z yyyy
   *
   * @return A String of the current UTC time (no offset).
   */
  public static String getUTC_time_server() {
    Date date = new Date();
    DateFormat timeFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
    TimeZone utcTime = TimeZone.getTimeZone("UTC");
    timeFormat.setTimeZone(utcTime);
    return timeFormat.format(date);
  }


  /**
   * Returns unique keys (e.g. filenames) based on current time, in format - <br>
   * <b>YMDHMS</b> <br>
   * Safe to one request per millisecond.
   *
   * @param now
   *
   * @return
   */
  public static String getYmdHms() {
    Calendar now = Calendar.getInstance();
    return getYmdHms(now);
  }


  /**
   * Returns timestamp in format - <br>
   * <b>YMDHMS</b> <br>
   * Safe to one request per millisecond.
   *
   * @param now
   *
   * @return
   */
  public static String getYmdHms(Calendar now) {
    String yy = df.format(now.get(Calendar.YEAR)).substring(2);
    String mo = df.format(now.get(Calendar.MONTH) + 1); // months 0-11 +1
    String dd = df.format(now.get(Calendar.DAY_OF_MONTH));

    String hh = df.format(now.get(Calendar.HOUR_OF_DAY));
    String mm = df.format(now.get(Calendar.MINUTE));
    String ss = df.format(now.get(Calendar.SECOND));

    return yy + mo + dd + "_" + hh + mm + ss;
  }


  /**
   * Returns unique keys (e.g. filenames) based on current time, in format - <br>
   * <b>YMDHMSmmm</b> <br>
   * Safe to one request per millisecond.
   *
   * @return
   */
  public static String getYmdKey() {
    Calendar now = Calendar.getInstance();
    return getYmdKey(now);
  }


  /**
   * Returns unique keys (e.g. filenames) based on current time, in format - <br>
   * <b>YMDHMSmmm</b> <br>
   * Safe to one request per millisecond.
   *
   * @param now
   *
   * @return
   */
  public static String getYmdKey(Calendar now) {
    String yy = df.format(now.get(Calendar.YEAR)).substring(2);
    String mo = df.format(now.get(Calendar.MONTH) + 1); // months 0-11 +1
    String dd = df.format(now.get(Calendar.DAY_OF_MONTH));

    String hh = df.format(now.get(Calendar.HOUR_OF_DAY));
    String mm = df.format(now.get(Calendar.MINUTE));
    String ss = df.format(now.get(Calendar.SECOND));

    String ms = df3.format(now.get(Calendar.MILLISECOND));

    return yy + mo + dd + hh + mm + ss + ms;
  }


  /**
   * Returns unique keys (e.g. filenames) based on current time. Safe to one request per millisecond.
   *
   * @return
   */
  public static String getYmdKey_zone() {
    Calendar now = Calendar.getInstance();
    return getYmdKey(now) + getZoneOffset(now);
  }


  /**
   * Calculates the time zone as UTC with offset in hours.
   *
   * @param now
   * @return The time zone only.
   */
  public static String getZoneOffset(Calendar now) {
    return "UTC" + now.get(Calendar.ZONE_OFFSET) / 3600000;
  }


  /**
   * Calculates the time zone as UTC with offset in hours.
   *
   * @param now
   * @return The time zone only.
   */
  public static String getZoneOffset_hhmm(Date date) {
    Calendar c = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
    c.setTime(date);

    return millisToHM(c.get(Calendar.ZONE_OFFSET));
  }


  private static String try_offset(Date date) {
    Calendar c = Calendar.getInstance();
    c.setTime(date);

    return millisToHM(c.get(Calendar.ZONE_OFFSET));
  }


  /**
   * Converts an elapsed HMS_ms in format <br>
   * <b>hh:mm:ss.SSS</b> <br>
   * to raw millis. Used to sum time intervals together.
   *
   * @param timestamp
   * @return
   */
  public static long hms_ms_to_millis(String time) {
    if (time == null || time.isEmpty() || time.equals("0"))
      return 0;

    String[] bits = time.split(":");

    int hours = Integer.parseInt(bits[0]);
    int mins = Integer.parseInt(bits[1]);

    bits = bits[2].split("\\.");
    int secs = Integer.parseInt(bits[0]);
    int millis = 0;
    if (bits.length > 1)
      millis = Integer.parseInt(bits[1]);

    long current_time = millis + (1000 * secs) + (1000 * 60 * mins) + (1000 * 3600 * hours);

    return current_time;
  }


  /**
   * Converts a timestamp in hh:mm:ss format to raw seconds. Used to calculate elapsed time of e.g. user-study tasks.
   * Does not factor in date and time zone.
   *
   * @param timestamp
   * @return
   */
  public static long hms_to_seconds(String time) {
    if (time == null || time.isEmpty())
      return 0;

    String[] bits = time.split(":");
    // Log.prbln("hm", bits);

    int hours = Integer.parseInt(bits[0]) * 3600;
    int mins = Integer.parseInt(bits[1]) * 60;
    int secs = 0;
    if (bits.length > 2)
      secs = Integer.parseInt(bits[2]);

    return hours + mins + secs;
  }


  // TODO TEST ONLY
  public static void main(String[] args) {

    // long ms = HMS_ms_ToMillis("329:57:32.964");
    // System.out.println(ms);
    // System.out.println(millisToHMS_ms(ms));

    // System.out.println(getMillis_MonthDay("12-10"));
    // System.out.println(getMillis_MonthDay("04-01"));

    // Date date = new Date();
    // System.out.println(getDateISO_UTC(date));

    // String date = "2013-12-04";
    // String time = "17:53:38.870";
    // System.out.println(getMillis_epochToStartOf(TimeUtils.DAY, date, time));
    // System.out.println(getMillis_epochToStartOf(TimeUtils.WEEK, date, time));
    // System.out.println(getMillis_epochToStartOf(TimeUtils.YEAR, date, time));
    // System.out.println();
    // time = "01:38:42.857";
    // System.out.println(getMillis_epochToStartOf(TimeUtils.DAY, date, time));
    // System.out.println(getMillis_epochToStartOf(TimeUtils.WEEK, date, time));
    // System.out.println(getMillis_epochToStartOf(TimeUtils.YEAR, date, time));

    // String date = "2013-12-01";
    // String time = "10:32:31";
    // System.out.println(getMillis_epochToStartOf(TimeUtils.WEEK, date, time));
    // time = "22:02:48";
    // System.out.println(getMillis_epochToStartOf(TimeUtils.WEEK, date, time));


    // String date = "2013-03-10";
    // String date2 = "2013-03-11";
    // System.out.println(getDaysBetween(date, date2));

    try_test();
  }


  /**
   * Given millis since the epoch, returns an array with date in ISO format yyyy-MM-dd and time hms.SSS, in <b>local
   * zone</b>.
   *
   * @param millis
   * @return
   */
  public static String[] millisToDateTime(long millis) {
    Calendar c = Calendar.getInstance();
    c.setTimeInMillis(millis);
    Date d = c.getTime();
    return new String[] { getDateISO(d), date_to_hms_ms(d) };
  }


  /**
   * Given millis since the epoch, returns an array with date in ISO format yyyy-MM-dd and time hms.SSS, in <b>zone
   * UTC</b>.
   *
   * @param millis
   * @return
   */
  public static String[] millisToDateTime_UTC(long millis) {
    Calendar c = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
    c.setTimeInMillis(millis);
    Date d = c.getTime();
    return new String[] { getUTC_dateISO(d), date_to_hms_ms_UTC(d) };
  }


  /**
   * Converts raw milliseconds into elapsed hours, minutes, seconds, and milliseconds in format - <br/>
   * <b>hh:mm:ss.SSS</b>
   *
   * @param millis
   *          A long
   * @return A String
   */
  public static String millisToHM(long millis) {
    int timeInSeconds, hours, minutes;
    timeInSeconds = (int) (Math.abs(millis) / 1000);
    hours = timeInSeconds / 3600;
    timeInSeconds = timeInSeconds - (hours * 3600);
    minutes = timeInSeconds / 60;
    return (millis < 0 ? "-" : "") //
        + String.format("%02d:%02d", hours, minutes);
  }


  /**
   * Converts raw milliseconds into elapsed hours, minutes, and seconds in format - <br/>
   * <b>HH h MM m SS s</b>
   *
   * @param millis
   * @return A String describing the elapsed time.
   */
  public static String millisToHMS(long millis) {
    int timeInSeconds, hours, minutes, seconds;
    timeInSeconds = (int) (Math.abs(millis) / 1000);
    hours = timeInSeconds / 3600;
    timeInSeconds = timeInSeconds - (hours * 3600);
    minutes = timeInSeconds / 60;
    timeInSeconds = timeInSeconds - (minutes * 60);
    seconds = timeInSeconds;
    return (millis < 0 ? "-" : "") //
        + hours + " h  " + minutes + " m  " + seconds + " s";
  }


  /**
   * Converts raw milliseconds into elapsed hours, minutes, seconds, and milliseconds in format - <br/>
   * <b>hh:mm:ss.SSS</b>
   *
   * @param millis
   *          A long
   * @return A String
   */
  public static String millisToHMS_ms(long millis) {
    int timeInSeconds, hours, minutes, seconds;
    timeInSeconds = (int) (Math.abs(millis) / 1000);
    hours = timeInSeconds / 3600;
    timeInSeconds = timeInSeconds - (hours * 3600);
    minutes = timeInSeconds / 60;
    timeInSeconds = timeInSeconds - (minutes * 60);
    seconds = timeInSeconds;
    return (millis < 0 ? "-" : "") //
        + String.format("%d:%02d:%02d.%03d", hours, minutes, seconds, Math.abs(millis) % 1000);
  }


  /**
   * Given millis since the epoch, returns a date string in ISO format yyyy-MM-dd, time zone UTC.
   *
   * @param millis
   * @return
   */
  public static String millisToYMD_UTC(long millis) {
    Calendar c = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
    c.setTimeInMillis(millis);
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    return sdf.format(c.getTime());
  }


  /**
   * Converts dates in format "January 1, 1970" into ISO format "1970-01-01".
   *
   * @param date
   * @return
   */
  public static String monthDDYY_to_ISO(String date) {
    if (tm_month_num == null) {
      tm_month_num = new TreeMap<String, String>();
      for (int i = 0; i < months.length; i++) {
        tm_month_num.put(months[i], df.format(i + 1));
      }
    }
    // Log.println(tm_month_num);
    String[] bits = date.split(",? +");
    // Log.prb(bits);
    return bits[2] + "-" + tm_month_num.get(bits[0]) + "-" + (bits[1].length() == 1 ? "0" + bits[1] : bits[1]);
  }


  private static void try_test() {
    String date = "2011-10-17t06:34:10z";

    Date d = tryDateFormats(date);
    String date_iso = TimeUtils.getDateISO_UTC(d);
    String time = TimeUtils.date_to_hms_ms_UTC(d);
    String zone = TimeUtils.getZoneOffset_hhmm(d);

    Log.println(date);
    Log.println(d);

    Calendar c = Calendar.getInstance();
    c.setTime(d);
    Log.println(c.getTimeInMillis());
    c.set(Calendar.ZONE_OFFSET, 0);
    Log.println(c.getTimeInMillis());

    Log.prbln("date     ", date_iso);
    Log.prbln("time     ", time);
    Log.prbln("zone     ", zone);
    int offset = c.get(Calendar.ZONE_OFFSET);
    Log.println(offset);
    Log.println(millisToHM(offset));
    Log.println(try_offset(d));
  }


  private static String[] date_formats = { //
      "EEE, d MMM yyyy HH:mm:ss Z", //
      "EEE MMM d HH:mm:ss Z yyyy", //
      "yyyy-MM-dd't'HH:mm:ss", //
      "yyyy-MM-dd't'HH:mm:ss Z", //
      "yyyy-MM-dd'T'HH:mm:ss Z", //
      "yyyy-MM-dd HH:mm:ss Z" };


  /**
   * Date parser. Tries the date string against common full-date formats.
   *
   * @param date
   * @return
   */
  public static Date tryDateFormats(String date) {
    if (date.toLowerCase().endsWith("z"))
      date = date.substring(0, date.length() - 1) + " +0000";
    Log.prbln("date", date);

    for (String df : date_formats)
      try {

        // Calendar cal = Calendar.getInstance();
        // SimpleDateFormat sdf = new SimpleDateFormat(df);
        // sdf.setCalendar(cal);
        // return sdf.parse(date);

        return new SimpleDateFormat(df).parse(date);

      } catch (Exception e) {
        // next ...
      }
    Log.println("WARN: TimeUtils: no matching date pattern");
    return null;
  }


  /**
   * Converts a timestamp in getTime_Seconds_Zone format <br>
   * yy/mm/dd <b>hh:mm:ss_zzz</b> <br>
   * to raw millis. Used to calculate elapsed time of e.g. user-study tasks. Does not factor in date and time zone.
   *
   * @param timestamp
   * @return
   */
  public static long YMD_HMS_zone_ToMillis(String timestamp) {
    String time = timestamp.split("_")[1]; // skip the date
    String[] bits = time.split("h:");

    int hours = Integer.parseInt(bits[0]) / 100;
    int mins = Integer.parseInt(bits[0]) % 100;

    bits = bits[1].split("\\.");
    int secs = Integer.parseInt(bits[0]);
    int millis = Integer.parseInt(bits[1]);

    long current_time = millis + (1000 * secs) + (1000 * 60 * mins) + (1000 * 3600 * hours);

    return current_time;
  }
}
