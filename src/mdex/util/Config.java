package mdex.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.TreeMap;


public class Config {


  public static HashMap<String, String> config;


  public static void add(String key,
                         String value) {
    config.put(key, value);
  }


  public static String get(String key) {
    return config.get(key);
  }


  public static boolean getBoolean(String str) {
    return Boolean.parseBoolean(get(str));
  }


  public static double getDouble(String str) {
    return Double.parseDouble(get(str));
  }


  /**
   * NOT USED
   * <p>
   * Gets the single-letter reference type labels used as suffixes for page numbers, as read from Config. For example,
   * the 'f' in 123f refers to a figure, 234t refers to a table, etc.
   *
   * @return All registered labels appended into a single String.
   */
  static String getTypeLabels() {
    Log.println("getTypeLabels");
    /* Index notations for figure, table, etc. */
    TreeMap<String, String> typeMap = loadMap("refType", "bobnet/config/notations.properties");

    // Get the notation labels
    StringBuilder sb = new StringBuilder();
    for (String v : typeMap.values()) {
      char c = v.charAt(v.length() - 1);
      if (Character.isLetter(c))
        sb.append(c);
    }
    return sb.toString();
  }


  /**
   * NB. If loading multiple Config files in the same JVM, for safety make sure that all config keys in all files are
   * unique. Otherwise, single-use values not assigned to Local variables may be over-written.
   *
   * @param filepath
   */
  public static void load(String filepath) throws Exception {
    Log.println(Config.class.getSimpleName() + "::load " + filepath);

    if (config == null)
      config = new HashMap<>();

    BufferedReader br = new BufferedReader(new FileReader(filepath));
    String line = "";
    while ((line = br.readLine()) != null) {
      line = line.trim();

      // NO! config lines may contain hash chars
      // line = line.split("#")[0]; // Ignore in-line comments

      if (line.trim().startsWith("#")) // skip comments
        continue;

      if (line.trim().isEmpty()) // skip comments
        continue;


      String[] bits = line.split("=", 2); // 2 bits only

      if (bits.length > 1) { // Single-line value
        config.put(bits[0].trim(), bits[1].trim());

      } else { // Multi-line value
        String key = line;
        StringBuilder val = new StringBuilder(" ");
        while (!(line = br.readLine()).trim().equals("@@@")) {
          val.append(line + " ");
        }
        config.put(key, val.toString());
      }

    }
    Log.println("config", config);
    br.close();

  }


  /**
   * Loads a map from a repeated key in a resource file. The instances of the key do not have to be adjacent. Skips
   * blank lines and comment lines (i.e. that start with '#').
   *
   * @param resKey
   *          The key to the desired map entries.
   * @param resFile
   *          The fully-qualified class name of the resource file.
   * @return A TreeMap containing the specified resource.
   */
  public static TreeMap<String, String> loadMap(String resKey,
                                                String resFile) {
    Log.prbln("Config::loadMap", resKey, resFile);

    ClassLoader loader = ClassLoader.getSystemClassLoader();
    InputStream in = loader.getResourceAsStream(resFile);
    BufferedReader br = new BufferedReader(new InputStreamReader(in));

    TreeMap<String, String> out = new TreeMap<String, String>();

    try {
      String line;
      while ((line = br.readLine()) != null) {
        if (line.isEmpty() || line.charAt(0) == '#')
          continue;

        String[] bits = line.split("=");
        String reskey = bits[0].trim();

        if (!reskey.equals(resKey))
          continue;

        String resvalue = bits[1].trim();

        Log.prb(reskey, resvalue);
        bits = resvalue.split(" +");
        String key = bits[0];
        String value = bits[1];

        out.put(key, value);
      }

    } catch (IOException e) {
      e.printStackTrace();
    }

    return out;
  }


  public static Integer getInteger(String string) {
    String s = get(string);
    if (s == null)
      return null;
    return Integer.parseInt(s);
  }


  public static int getInt(String string) {
    return getInteger(string);
  }

}
