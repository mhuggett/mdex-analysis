package mdex.util;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;


public class SystemUtils {


  /**
   * Executes the given command/script w/ Runtime.
   *
   * @param cmd
   *          The script to execute.
   * @return A String of the command's output.
   */
  public static String exec(String cmd) {
    Log.println(SystemUtils.class.getSimpleName() + "::exec_script " + cmd);

    Runtime r = Runtime.getRuntime();

    StringBuilder sb = new StringBuilder();

    try {
      Process p = r.exec(cmd);

      // Read the cmd output
      String line;

      BufferedReader br_in = new BufferedReader(new InputStreamReader(new BufferedInputStream(p.getInputStream())));
      while ((line = br_in.readLine()) != null) {
        Log.println(line);
        sb.append(line).append("\n");
      }

      BufferedReader br_err = new BufferedReader(new InputStreamReader(new BufferedInputStream(p.getErrorStream())));
      while ((line = br_err.readLine()) != null) {
        Log.println(line);
        sb.append(line).append("\n");
      }

      // Check for failure
      try {
        if (p.waitFor() != 0)
          Log.println("WARNING: SystemUtils::doCall exit value = " + p.exitValue());

      } catch (InterruptedException e) {
        System.err.println(e);

      } finally {
        br_in.close();
      }
    } catch (IOException e) {
      Log.println(e);
    }

    sb.setLength(sb.length() - 1); // remove last \n
    return sb.toString();
  }


  /**
   * Executes the given command/script within the specified directory.
   *
   * @param dir
   *          Directory
   * @param cmd
   *          Command
   * @return
   */
  public static String exec(String dir,
                            String cmd) {
    Log.println(SystemUtils.class.getSimpleName() + "::exec_cmd ", dir, cmd);

    StringBuilder sb = new StringBuilder();

    try {
      ProcessBuilder pb = new ProcessBuilder(cmd);
      pb.directory(new File(dir));
      Process p = pb.start();

      // Read the cmd output
      String line;

      BufferedReader br_in = new BufferedReader(new InputStreamReader(new BufferedInputStream(p.getInputStream())));
      while ((line = br_in.readLine()) != null) {
        Log.println(line);
        sb.append(line).append("\n");
      }

      BufferedReader br_err = new BufferedReader(new InputStreamReader(new BufferedInputStream(p.getErrorStream())));
      while ((line = br_err.readLine()) != null) {
        Log.println(line);
        sb.append(line).append("\n");
      }

      // Check for failure
      try {
        if (p.waitFor() != 0)
          Log.println("WARNING: SystemUtils::doCall exit value = " + p.exitValue());

      } catch (InterruptedException e) {
        System.err.println(e);

      } finally {
        br_in.close();
      }
    } catch (IOException e) {
      Log.println(e);
    }

    sb.setLength(sb.length() - 1); // remove last \n
    return sb.toString();
  }


  /**
   * Executes the given command/script as a bash process.
   * <p>
   * Will interpret the command as : <br>
   * String[] cmd_arr = { "/bin/bash", "-c", cmd };
   * <p>
   * Can handle commands such as: "netstat -n | head -3 | tail -1"
   *
   * @param cmd
   *          The command to execute.
   * @return A String of the command's output.
   */
  public static String exec_bash(String cmd) throws Exception {
    Log.println(SystemUtils.class.getSimpleName() + "::exec " + cmd);

    String[] cmd_arr = { "/bin/bash", "-c", cmd };

    Runtime r = Runtime.getRuntime();

    StringBuilder sb = new StringBuilder();

    Process p = r.exec(cmd_arr);

    // Read the cmd output
    String line;

    BufferedReader br_in = new BufferedReader(new InputStreamReader(new BufferedInputStream(p.getInputStream())));
    while ((line = br_in.readLine()) != null)
      sb.append(line).append("\n");
    br_in.close();

    BufferedReader br_err = new BufferedReader(new InputStreamReader(new BufferedInputStream(p.getErrorStream())));
    while ((line = br_err.readLine()) != null)
      sb.append(line).append("\n");
    br_err.close();

    // Check for ls failure
    if (p.waitFor() != 0)
      Log.println("WARNING: SystemUtils::doCall exit value = " + p.exitValue());

    sb.setLength(sb.length() - 1); // remove last \n
    return sb.toString();
  }


  /**
   * Prints to log: home_dir, work_dir, user_name, and OS details.
   */
  public static void print_banner() {
    Log.prb("user.home", System.getProperty("user.home"));
    Log.prb("user.dir", System.getProperty("user.dir"));
    Log.prb("user.name", System.getProperty("user.name"));
    Log.prb("os.name", System.getProperty("os.name"));
    Log.prb("os.arch", System.getProperty("os.arch"));
    Log.prb("os.version", System.getProperty("os.version"));
    Log.prb("file.encoding", System.getProperty("file.encoding"));
  }

}
